package com.example.notificiaonfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("enviaratodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //Toast.makeText(MainActivity.this, "suscrito", Toast.LENGTH_LONG).show();
            }
        });



        Button general = (Button) findViewById(R.id.btnGeneral);
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                general();
            }
        });

        Button Especifico = (Button) findViewById(R.id.btnEspecifico);
        Especifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Especifico();
            }
        });

    }

    public void general(){

        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            //String token = "fWNwrmSr4xk:APA91bHz-goJ4mMho_rCJnrkp2t_hOe-gKDmNiQNkUJBARTnBqxhU2xW0FBbD_ehieGcCAhIq4xi_8UDjZvfkHW9kCnMlCLsAkBl23RVT4GPds5C-BqUjp1LqmDrL6vj1a8Cc_Yx52Pn";
            json.put("to","/topics"+"enviaratodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo","HOLA");
            notificacion.put("detalle","ESTE ES EL CONTENIDO");
            notificacion.put("folio","1234");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json, null,null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String,String> header= new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAzEOVyLI:APA91bFXAu9UWi4nxM3F0ZROXXgK5w33YTVdTutARpBsZRDsflnG9d_8t0ynOop4PG-OYCw4-_edlq2pGV6Qn1IvvxqqifVRIQ5qLDbejvnKm-ye4Bf2l4-Q90RYXupvg-Af0o5RIUOv");
                    return header;
                }
            };
            myrequest.add(request);


        }catch (JSONException e){
            e.printStackTrace();
        }
    }
    public void Especifico(){
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            String token = "fWNwrmSr4xk:APA91bHz-goJ4mMho_rCJnrkp2t_hOe-gKDmNiQNkUJBARTnBqxhU2xW0FBbD_ehieGcCAhIq4xi_8UDjZvfkHW9kCnMlCLsAkBl23RVT4GPds5C-BqUjp1LqmDrL6vj1a8Cc_Yx52Pn";
            json.put("to",token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo","HOLA");
            notificacion.put("detalle","ESTE ES EL CONTENIDO");
            notificacion.put("folio","1234");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json, null,null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String,String> header= new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAzEOVyLI:APA91bFXAu9UWi4nxM3F0ZROXXgK5w33YTVdTutARpBsZRDsflnG9d_8t0ynOop4PG-OYCw4-_edlq2pGV6Qn1IvvxqqifVRIQ5qLDbejvnKm-ye4Bf2l4-Q90RYXupvg-Af0o5RIUOv");
                    return header;
                }
            };
            myrequest.add(request);


        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}